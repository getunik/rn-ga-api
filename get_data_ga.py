# mainly taken from following instructions https://medium.com/analytics-for-humans/submitting-your-first-google-analytics-reporting-api-request-cdda19969940
import json
from pandas.io.json import json_normalize
import pandas as pd
from sklearn import datasets
import time
#Packages needed for authentication
import httplib2 as lib2 #Example of the "as" function
from oauth2client import client #Importing a sub-package
#Packages needed for connecting with Google API
from googleapiclient.discovery import build as google_build #An example with all the statements together
#Data processing packages
import numpy
from datetime import datetime, timedelta #importing multiple sub-packages from one package
import argparse


def request_ga(next_token, ga_view, start_date, end_date, api_client, metrics, dimensions):

    sample_request = {
      'viewId': ga_view,
      'pageSize': 10000,
      'dateRanges': {
          'startDate': start_date,
          #datetime.strftime(datetime.now() - timedelta(days = 30),'%Y-%m-%d'),
          'endDate':end_date
          #datetime.strftime(datetime.now(),'%Y-%m-%d')
          },
      'dimensions': dimensions, #,{'name': 'ga:clientID'}
      'metrics': metrics,

    "pageToken": (next_token)
       # 'filters': [{'expression': 'ga:itemRevenue'}]
    }

    response = api_client.reports().batchGet(
      body={
        'reportRequests': sample_request
      }).execute()


    return response.get('reports', [])[0]

#Parse the response of API
def prase_response(report):

    """Parses and prints the Analytics Reporting API V4 response"""
    #Initialize results, in list format because two dataframes might return
    result_list = []

    #Initialize empty data container for the two dateranges (if there are two that is)
    data_csv = []
    data_csv2 = []

    #Initialize header rows
    header_row = []

    #Get column headers, metric headers, and dimension headers.
    columnHeader = report.get('columnHeader', {})
    metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])
    dimensionHeaders = columnHeader.get('dimensions', [])

    #Combine all of those headers into the header_row, which is in a list format
    for dheader in dimensionHeaders:
        header_row.append(dheader)
    for mheader in metricHeaders:
        header_row.append(mheader['name'])

    #Get data from each of the rows, and append them into a list
    rows = report.get('data', {}).get('rows', [])
    for row in rows:
        row_temp = []
        dimensions = row.get('dimensions', [])
        metrics = row.get('metrics', [])
        for d in dimensions:
            row_temp.append(d)
        for m in metrics[0]['values']:
            row_temp.append(m)
            data_csv.append(row_temp)

        #In case of a second date range, do the same thing for the second request
        if len(metrics) == 2:
            row_temp2 = []
            for d in dimensions:
                row_temp2.append(d)
            for m in metrics[1]['values']:
                row_temp2.append(m)
            data_csv2.append(row_temp2)

    #Putting those list formats into pandas dataframe, and append them into the final result
    result_df = pd.DataFrame(data_csv, columns=header_row)
    result_list.append(result_df)
    if data_csv2 != []:
        result_list.append(pd.DataFrame(data_csv2, columns=header_row))

    return result_list

def main(ga_view, start_date, end_date, access_token, refresh_token, client_id, client_secret, metrics = [{'expression': 'ga:itemRevenue'}],
 dimensions = [{'name': 'ga:transactionId'},{'name': 'ga:dateHourMinute'},{'name': 'ga:productSku'},
                     {'name': 'ga:browser'},{'name': 'ga:browserVersion'},{'name': 'ga:operatingSystem'},
                     {'name': 'ga:operatingSystemVersion'},{'name': 'ga:screenResolution'}]):

    """
    Follwoing arguments are obligatory: a_view, start_date, end_date, access_token, refresh_token, client_id, client_secret
    --
    Optional metrics and dimensions: 
    metrics = [{'expression': 'ga:itemRevenue'}],
    dimensions = [{'name': 'ga:transactionId'},{'name': 'ga:dateHourMinute'},{'name': 'ga:productSku'},
                     {'name': 'ga:browser'},{'name': 'ga:browserVersion'},{'name': 'ga:operatingSystem'},
                     {'name': 'ga:operatingSystemVersion'},{'name': 'ga:screenResolution'}]
    """
    #This is consistent for all Google services
    token_uri = 'https://accounts.google.com/o/oauth2/token'

    #We are essentially setting the expiry date to 1 day before today, which will make it always expire
    token_expiry = datetime.now() - timedelta(days = 1)

    #¯\_(ツ)_/¯
    user_agent = 'my-user-agent/1.0'


    #The real code that initalized the client
    credentials = client.GoogleCredentials(access_token=access_token, refresh_token=refresh_token, 
                                        client_id=client_id, client_secret=client_secret, 
                                        token_uri=token_uri, token_expiry=token_expiry, 
                                        user_agent=user_agent)

    #Initialize Http Protocol    
    http = lib2.Http()

    #Authorize client
    authorized = credentials.authorize(http)

    #API Name and Verison, these don't change until 
    #they release a new API version for us to play with. 
    api_name = 'analyticsreporting'
    api_version = 'v4'

    #Let's build the client
    api_client = google_build(serviceName=api_name, version=api_version, http=authorized)

    # this section seems inefficient, would probably be better to 1. get all data, 2. parse and 3. combine into DF
    response_data = request_ga('0', ga_view, start_date, end_date, api_client, metrics, dimensions)
    response_ga=prase_response(response_data)[0]
    ga_all= pd.DataFrame(response_ga)

    return ga_all


if __name__ == '__main__':   
    main(ga_view, start_date, end_date, access_token, refresh_token, client_id, client_secret)

