import get_data_ga
import get_data_rn
import datetime
from dotenv import load_dotenv
import os
import pandas as pd
import matplotlib.pyplot as plt

client = 'HEILS'
load_dotenv(client + '.env')
ga_view = os.getenv('GA_VIEW')
ga_access_token =  os.getenv('GA_ACCESS_TOKEN')
ga_refresh_token =  os.getenv('GA_REFRESH_TOKEN')
ga_client_id = os.getenv('GA_CLIENT_ID')
ga_client_secrect = os.getenv('GA_CLIENT_SECRET')
ga_start_date = os.getenv('GA_START_DATE')
ga_end_date = os.getenv('GA_END_DATE')
rn_token = os.getenv('RN_TOKEN')
rn_merchant = os.getenv('RN_MERCHANT')
rn_merchant_config_id = os.getenv('RN_MERCHANT_CONFIG_ID')
rn_start_date = os.getenv('RN_START_DATE')
rn_end_date = os.getenv('RN_END_DATE')


# Function to transform date unix time stamp
def to_unix(time_column):
    unixtime = time.mktime(datetime.datetime.strptime (time_column, "%Y%m%d%H%M").timetuple())
    return unixtime

def from_unix(unix_column):
    date_time = datetime.datetime.utcfromtimestamp(int(unix_column)).strftime('%Y-%m-%d %H:%M:%S')
    return date_time



ga_all = get_data_ga.main(
    ga_view = ga_view, 
    start_date = ga_start_date, 
    end_date = ga_end_date, 
    access_token = ga_access_token, 
    refresh_token = ga_refresh_token, 
    client_id = ga_client_id, 
    client_secret = ga_client_secrect,
    metrics = [{'expression': 'ga:itemRevenue'}],
    dimensions = [{'name': 'ga:transactionId'},{'name': 'ga:dateHourMinute'},{'name': 'ga:productSku'},
                        {'name': 'ga:browser'},{'name': 'ga:browserVersion'},{'name': 'ga:operatingSystem'},
                        {'name': 'ga:operatingSystemVersion'},{'name': 'ga:screenResolution'},
                        {'name': 'ga:hostname'}])
print(ga_all.head())

rn_all = get_data_rn.main(\
    token = rn_token,
    start_datehour = rn_start_date,                     # enter start date hour as number (int) 20180101000000
    end_datehour = rn_end_date,                       # enter end date hour as number (int) 20181231235959
    merchant = rn_merchant,  
    merchant_config_id = rn_merchant_config_id,
    displayed_fields = 'amount,created,currency_identifier,epp_transaction_id,last_status,payment_method_identifier,stored_rnw_source_url,user_agent,used_redirect_url,success_url,stored_rnw_product_name,mobile_mode')

print(rn_all.head())

print('------------------------------')
print(client, 'Start date: ', ga_start_date, ' end date: ', ga_end_date)

print('------------------------------')
# this code creates new column match_RN_ID and fills it with 1 where we have a matching Raisenow ID else 0
ga_all['match_RN_ID'] = 0
ga_all['match_RN_ID'] = ga_all['ga:transactionId'].isin(rn_all.epp_transaction_id).astype(int)
print('GA matching RN ID: ', ga_all['match_RN_ID'].value_counts())

print('------------------------------')
# this code creates new column match_RN_ID and fills it with 1 where we have a matching Raisenow ID else 0
rn_all['match_GA_ID'] = 0
rn_all['match_GA_ID'] = rn_all['epp_transaction_id'].isin(ga_all['ga:transactionId']).astype(int)
#rn_all = rn_all.loc[ (rn_all['domain'] == 'helfen.vier-pfoten.ch') | (rn_all['domain'] == 'aider.quatre-pattes.ch') ]
print('RN matching GA ID: ', rn_all['match_GA_ID'].value_counts())

print('------------------------------')
#combined_matched_unmatched
print(client, 'RN_unmatched: ' + str(len(rn_all[rn_all['match_GA_ID'] == 0])) + ' combined_matched: ' + str(len(rn_all[rn_all['match_GA_ID'] == 1])))
print('Percentage Trans matched: ' + str(round( (1 - len(rn_all[rn_all['match_GA_ID'] == 0])/
(len(rn_all[rn_all['match_GA_ID'] == 1]) +
len(rn_all[rn_all['match_GA_ID'] == 0])))* 100, 2)) + ' %')

print('------------------------------')

print(client, 'RN_unmatched CHF: ' + str(rn_all.loc[(rn_all['match_GA_ID'] == 0) ]['amount'].sum()) + ' combined_matched CHF: ' + str(rn_all[rn_all['match_GA_ID'] == 1].amount.sum()))
print('Percentage Revenue matched: ' + str(round( (1 - rn_all[rn_all['match_GA_ID'] == 0]['amount'].sum()/
(rn_all[rn_all['match_GA_ID'] == 1]['amount'].sum() +
rn_all[rn_all['match_GA_ID'] == 0]['amount'].sum()))* 100, 2)) + ' %')

print('------------------------------')

# print('Double entries: ' + str(len(pd.merge(ga_all,rn_all, left_on='ga:transactionId', right_on= 'epp_transaction_id', how='outer'))))

# print('------------------------------')

#gu_colours = [(0, 106, 182),(240, 69, 78),(250, 165, 25),(39, 140, 203), (243, 121, 120), (243, 121, 120), (255, 199, 0)]
gu_colours = ['#006AB6', '#F0454E', '#FAA519', '#278CCB','#F37978', '#FFC700']

plt.figure(1)
rn_all[rn_all['match_GA_ID'] == 1]['payment_method_identifier'].value_counts().plot.bar( title = f'{client} GA tracked payment methods', rot = 0, color = gu_colours)
plt.figure(2)
rn_all[rn_all['match_GA_ID'] == 0]['payment_method_identifier'].value_counts().plot.bar( title = f'{client} payment methods not tracked in GA', rot = 0, color = gu_colours)
try:
    plt.figure(3)
    rn_all[rn_all['match_GA_ID'] == 1]['mobile_detected'].value_counts().plot.bar( title = f'{client} tracked by GA: Mobile Device', rot = 0, color = gu_colours)
    plt.figure(4)
    rn_all[rn_all['match_GA_ID'] == 0]['mobile_detected'].value_counts().plot.bar( title = f'{client} not tracked by GA: Mobile Device', rot = 0, color = gu_colours)
    plt.figure(5)
    rn_all[rn_all['match_GA_ID'] == 1]['domain'].value_counts().plot.bar( title = f'{client} GA tracked by hostname', rot = 0, color = gu_colours)
    plt.xticks(rotation= 90)
    plt.figure(6)
    rn_all[rn_all['match_GA_ID'] == 0]['domain'].value_counts().plot.bar( title = f'{client} GA not tracked by hostname', rot = 0, color = gu_colours)
    plt.xticks(rotation= 90)


except Exception as e:
            print(e)

# Excel of matched and unmatched
match_RN_ID = pd.merge(rn_all, ga_all, left_on= 'epp_transaction_id', right_on='ga:transactionId', how='outer') 
match_RN_ID.to_excel(f'data/{client}_{ga_start_date}_{ga_end_date}_combined_matched.xlsx')
plt.show()
