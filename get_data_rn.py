# load libraries

import base64
import requests
import json
from pandas.io.json import json_normalize
import pandas as pd
from sklearn import datasets
from datetime import datetime, timedelta
import time
import re
from user_agents import parse
from tld import get_tld

# reg_b = re.compile(r"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino", re.I|re.M)
# reg_v = re.compile(r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-", re.I|re.M)

# def detect_mobile_browser(user_agent):
#     b = reg_b.search(user_agent)
#     v = reg_v.search(user_agent[0:4])
#     if b or v:
#         return True
#     else:
#         return False

# code was sent by Marco Streich (Raisenow) it uses an older API version not v2

def fetch_source_records(url, payload, entity, token):
    response_result_json = requests.get(url, params=payload, headers={"Authorization": "Basic " + token}).json().get('result', {})
    fetched_source_records = response_result_json.get(entity, [])
    no_of_pages = response_result_json.get('additional_info').get('total_pages', None)
    scroll_id = response_result_json.get('additional_info').get('scroll_id', None)

    print("Fetched " + str(len(fetched_source_records)) + " " + entity)

    return {'fetched_source_records': fetched_source_records, 'no_of_pages': no_of_pages, 'scroll_id': scroll_id}


def date_to_string(date):
    return date.strftime("%Y%m%d%H%M%S")

def main(token, merchant, start_datehour, end_datehour, merchant_config_id = 0,
    displayed_fields = 'amount,created,currency_identifier,epp_transaction_id,last_status,payment_method_identifier,stored_rnw_source_url,user_agent,used_redirect_url,success_url'):
    """
    Follwoing arguments are obligatory: 
    token is RN username:password encoded in Base64, 
    merchant (in contrast to RN API v2 this code requires the merchant id not the api key), 
    start_datehour (start date hour as number (int) 20180101000000), 
    end_datehour (end date hour as number (int) 20181231235959)
    --
    Optional metrics and dimensions: 
    displayed_fields
    """
    time_start = time.time()

    rn_payload = {
        'records_per_page': 1000,
        'displayed_fields': displayed_fields,
        'sort[0][field_name]': 'created',
        'sort[0][order]': 'desc',
        'filters[0][field_name]': 'test_mode',
        'filters[0][type]': 'term',
        'filters[0][value]': 'production',
        'filters[1][field_name]': 'last_status',
        'filters[1][type]': 'term',
        'filters[1][value]': 'final_success',
        'filters[2][field_name]': 'created',
        'filters[2][type]': 'date_range',
        'filters[2][from]': start_datehour,
        'filters[2][to]': end_datehour,
        'filters[3][field_name]': 'is_recurring',
        'filters[3][type]': 'term',
        'filters[3][value]': 'false',

    }

    if isinstance(merchant_config_id, str):
        rn_payload['filters[4][field_name]'] = 'merchant_config_identifier'
        rn_payload['filters[4][type]'] = 'term'
        rn_payload['filters[4][value]'] = merchant_config_id


    source_entity = 'transactions'

    api_url = 'https://api.raisenow.com/epayment/api' + (('/' + merchant) if merchant else '') + ('/' + source_entity)
    search_api_url = api_url + "/search"
    scroll_api_url = api_url + "/scroll"

    fields = displayed_fields

    payload_rnw_search = rn_payload
    payload_rnw_scroll = {
        'displayed_fields': fields,
        'records_per_page': 1000,
        'scroll_id': None
    }

    response = fetch_source_records(url = search_api_url, payload = {**payload_rnw_search, 'scroll': 'true'}, entity = source_entity, token = token)

    records = []
    records.extend(response.get('fetched_source_records', []))

    pages = response['no_of_pages']
    scroll_id = response['scroll_id']

    if pages > 1:
        payload_rnw_scroll['scroll_id'] = scroll_id
        for page in range(1, pages):
            records.extend(
                fetch_source_records(scroll_api_url, payload_rnw_scroll, source_entity, token).get('fetched_source_records', []))

    r = json.dumps(records)

    print("Fetched a total of " + str(len(records)) + " " + source_entity)

    print("\n\n### execution time: %s seconds" % (time.time() - time_start))

    # change data into dataframe
    rn_all = pd.DataFrame(records)
    rn_all['amount'] = rn_all['amount']/100
    try:
        rn_all['simple_user_agent'] = rn_all['user_agent'].apply(lambda x: parse(x) if isinstance(x, str) else 0)
    except Exception as e:
        print(e)
    try:
        rn_all['mobile_detected'] = rn_all['user_agent'].apply(lambda x: parse(x).is_mobile if isinstance(x, str) else 0)
    except Exception as e:
        print(e)
    try:
        rn_all['browser_detected'] = rn_all['user_agent'].apply(lambda x: parse(x).browser.family if isinstance(x, str) else 0)
    except Exception as e:
        print(e)
    try:
         rn_all['domain'] = rn_all['success_url'].apply(lambda x: get_tld(x , as_object=True).parsed_url[1] if isinstance(x, str) else 0)
    except Exception as e:
        print(e)
   


    return rn_all

if __name__ == '__main__':   
    main(token, merchant, start_datehour, end_datehour, merchant_config_id, displayed_fields)
